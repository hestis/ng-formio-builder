formData = JSON.parse(`
{
	"components": [{
		"key": "TextFieldPanel",
		"input": false,
		"title": "TextField",
		"theme": "primary",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputType": "text",
			"inputMask": "",
			"label": "First Name",
			"key": "TextFieldPanelFirstName",
			"placeholder": "Enter your first name",
			"prefix": "",
			"suffix": "",
			"multiple": false,
			"defaultValue": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"validate": {
				"required": true,
				"minLength": "",
				"maxLength": "",
				"pattern": "",
				"custom": "",
				"customPrivate": false
			},
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"type": "textfield",
			"hidden": false,
			"clearOnHide": true,
			"$$hashKey": "object:405",
			"tags": [],
			"properties": {
				"": ""
			}
		}, {
			"input": true,
			"tableView": true,
			"inputType": "text",
			"inputMask": "",
			"label": "Last Name",
			"key": "TextFieldPanelLastName",
			"placeholder": "Enter your last name",
			"prefix": "",
			"suffix": "",
			"multiple": false,
			"defaultValue": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"validate": {
				"required": false,
				"minLength": "",
				"maxLength": "",
				"pattern": "",
				"custom": "",
				"customPrivate": false
			},
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"type": "textfield",
			"hidden": false,
			"clearOnHide": true,
			"$$hashKey": "object:455",
			"tags": [],
			"properties": {
				"": ""
			},
			"description": "Not required"
		}, {
			"input": true,
			"tableView": true,
			"inputType": "text",
			"inputMask": "999.999.999-99",
			"label": "Social ID",
			"key": "textFieldPanelSocialId",
			"placeholder": "",
			"prefix": "CPF",
			"suffix": "",
			"multiple": false,
			"defaultValue": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"minLength": "",
				"maxLength": "",
				"pattern": "",
				"custom": "",
				"customPrivate": false
			},
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"type": "textfield",
			"$$hashKey": "object:766",
			"tags": [],
			"properties": {
				"": ""
			}
		}, {
			"input": true,
			"tableView": true,
			"inputType": "text",
			"inputMask": "",
			"label": "Multiple textfield",
			"key": "textFieldPanelMultipletextfield",
			"placeholder": "Multiple textfield",
			"prefix": "",
			"suffix": "",
			"multiple": true,
			"defaultValue": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"minLength": "",
				"maxLength": "",
				"pattern": "",
				"custom": "",
				"customPrivate": false
			},
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"type": "textfield",
			"$$hashKey": "object:1830",
			"tags": [],
			"properties": {
				"": ""
			},
			"description": "Multiple textfield"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:249",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"lockKey": true
	}, {
		"key": "NumberPanel",
		"input": false,
		"title": "Number",
		"theme": "info",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputType": "number",
			"label": "Age",
			"key": "numberPanelAge",
			"placeholder": "Select your age",
			"prefix": "",
			"suffix": "Years Old",
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"min": "",
				"max": "",
				"step": "any",
				"integer": "",
				"multiple": "",
				"custom": ""
			},
			"type": "number",
			"$$hashKey": "object:1417",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "You may use the arrows to select date"
		}, {
			"input": true,
			"tableView": true,
			"inputType": "number",
			"label": "Increment by 2",
			"key": "numberPanelOnlypairnumbers",
			"placeholder": "",
			"prefix": "",
			"suffix": "",
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"min": "",
				"max": "",
				"step": "2",
				"integer": "",
				"multiple": "",
				"custom": ""
			},
			"type": "number",
			"$$hashKey": "object:1790",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:1128",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"lockKey": true
	}, {
		"key": "undefinedUndefinedUndefinedPanel",
		"input": false,
		"title": "Password",
		"theme": "success",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": false,
			"inputType": "password",
			"label": "Weak Password",
			"key": "PanelWeakPassword",
			"placeholder": "type any password",
			"prefix": "",
			"suffix": "",
			"protected": true,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"type": "password",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "It does not matter",
			"lockKey": true,
			"$$hashKey": "object:2751"
		}, {
			"input": true,
			"tableView": false,
			"inputType": "password",
			"label": "Stronger password",
			"key": "undefinedUndefinedUndefinedPanelStrongerpassword",
			"placeholder": "",
			"prefix": "",
			"suffix": "",
			"protected": true,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"type": "password",
			"$$hashKey": "object:2846",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "expect at least one uppercase character",
			"validate": {
				"custom": "valid = /[A-Z]/.test(input) || 'Should have at least one uppercase character'"
			},
			"customError": "Should have at least one uppercase character",
			"isNew": false
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:2740"
	}, {
		"key": "TextAreaPanel",
		"input": false,
		"title": "Text Area",
		"theme": "danger",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Text Area",
			"key": "textAreaPanelTextArea",
			"placeholder": "Describe what you wish...",
			"prefix": "",
			"suffix": "",
			"rows": 3,
			"multiple": false,
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"wysiwyg": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"minLength": "",
				"maxLength": "",
				"pattern": "",
				"custom": ""
			},
			"type": "textarea",
			"$$hashKey": "object:4038",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": ""
		}, {
			"input": true,
			"tableView": true,
			"label": "WYSIWYG Text Area",
			"key": "textAreaPanelWysiwygTextArea",
			"placeholder": "This is a WYSIWYG Editor",
			"prefix": "",
			"suffix": "",
			"rows": 3,
			"multiple": false,
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"wysiwyg": {
				"toolbarGroups": [{
					"name": "basicstyles",
					"groups": ["basicstyles", "cleanup"]
				}, {
					"name": "paragraph",
					"groups": ["list", "indent", "blocks", "align", "bidi", "paragraph", "-", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"]
				}, {
					"name": "links",
					"groups": ["links"]
				}, {
					"name": "insert",
					"groups": ["insert"]
				}, "/", {
					"name": "styles",
					"groups": ["Styles", "Format", "Font", "FontSize"]
				}, {
					"name": "colors",
					"groups": ["colors"]
				}, {
					"name": "clipboard",
					"groups": ["clipboard", "undo"]
				}, {
					"name": "editing",
					"groups": ["find", "selection", "spellchecker", "editing"]
				}, {
					"name": "document",
					"groups": ["mode", "document", "doctools"]
				}, {
					"name": "others",
					"groups": ["others"]
				}, {
					"name": "tools",
					"groups": ["tools"]
				}],
				"extraPlugins": "justify,font",
				"removeButtons": "Cut,Copy,Paste,Underline,Subscript,Superscript,Scayt,About",
				"uiColor": "#eeeeee",
				"height": "400px",
				"width": "100%"
			},
			"clearOnHide": true,
			"validate": {
				"required": false,
				"minLength": "",
				"maxLength": "",
				"pattern": "",
				"custom": ""
			},
			"type": "textarea",
			"$$hashKey": "object:4347",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "CKEDITOR"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:3480",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"lockKey": true
	}, {
		"key": "undefinedPanel",
		"input": false,
		"title": "Check Boxes",
		"theme": "warning",
		"tableView": false,
		"components": [{
			"input": true,
			"inputType": "checkbox",
			"tableView": true,
			"label": "Check Box",
			"datagridLabel": true,
			"key": "PanelCheckBox",
			"defaultValue": false,
			"protected": false,
			"persistent": true,
			"hidden": false,
			"name": "",
			"value": "",
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "checkbox",
			"$$hashKey": "object:5924",
			"hideLabel": true,
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"lockKey": true
		}, {
			"input": true,
			"inputType": "radio",
			"tableView": true,
			"label": "Check Box as Radio",
			"datagridLabel": true,
			"key": "PanelCheckBoxasRadio",
			"defaultValue": false,
			"protected": false,
			"persistent": true,
			"hidden": false,
			"name": "",
			"value": "RadioValue",
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "checkbox",
			"$$hashKey": "object:6268",
			"hideLabel": true,
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"lockKey": true
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:5630",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedPanel2",
		"input": false,
		"title": "Select Boxes",
		"theme": "default",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Select Boxes Component",
			"key": "undefinedPanel2SelectBoxesComponent",
			"values": [{
				"value": "one",
				"label": "One"
			}, {
				"value": "two",
				"label": "Two"
			}, {
				"value": "tree",
				"label": "Tree"
			}],
			"inline": false,
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "selectboxes",
			"$$hashKey": "object:7483",
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:7048",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedPanel3",
		"input": false,
		"title": "Select",
		"theme": "primary",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Select",
			"key": "Panel3Select",
			"placeholder": "Choose an option",
			"data": {
				"values": [{
					"value": "one",
					"label": "One",
					"$$hashKey": "object:8672"
				}, {
					"value": "two",
					"label": "Two",
					"$$hashKey": "object:8685"
				}, {
					"value": "tree",
					"label": "Tree",
					"$$hashKey": "object:8689"
				}],
				"json": "",
				"url": "",
				"resource": "",
				"custom": ""
			},
			"dataSrc": "values",
			"valueProperty": "",
			"defaultValue": "",
			"refreshOn": "",
			"filter": "",
			"authenticate": false,
			"template": "<span>{{ item.label }}</span>",
			"multiple": false,
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "select",
			"$$hashKey": "object:8439",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Select one",
			"lockKey": true
		}, {
			"type": "select",
			"validate": {
				"required": false
			},
			"clearOnHide": true,
			"persistent": true,
			"unique": false,
			"protected": false,
			"multiple": true,
			"template": "<span>{{ item.label }}</span>",
			"authenticate": false,
			"filter": "",
			"refreshOn": "",
			"defaultValue": "",
			"valueProperty": "",
			"dataSrc": "values",
			"data": {
				"custom": "",
				"resource": "",
				"url": "",
				"json": "",
				"values": [{
						"label": "Raindrops on roses",
						"value": "raindropsOnRoses",
						"$$hashKey": "object:17105"
					}, {
						"label": "Whiskers on Kittens",
						"value": "whiskersOnKittens",
						"$$hashKey": "object:17106"
					}, {
						"label": "Bright copper kettles",
						"value": "brightCopperKettles",
						"$$hashKey": "object:17107"
					}, {
						"label": "Warm woolen Mittens",
						"value": "warmWoolenMittens",
						"$$hashKey": "object:17108"
					},
					[]
				]
			},
			"placeholder": "Select a few",
			"key": "undefinedPanel3FavoriteThings",
			"label": "Favorite Things",
			"tableView": true,
			"input": true,
			"hidden": false,
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:16869"
		}, {
			"input": true,
			"tableView": true,
			"label": "Select From Api",
			"key": "undefinedPanel3SelectFromApi",
			"placeholder": "",
			"data": {
				"values": [{
					"value": "",
					"label": "",
					"$$hashKey": "object:15588"
				}],
				"json": "",
				"url": "https://jsonplaceholder.typicode.com/users",
				"resource": "",
				"custom": "",
				"headers": [{
					"value": "",
					"key": ""
				}]
			},
			"dataSrc": "url",
			"valueProperty": "",
			"defaultValue": "",
			"refreshOn": "",
			"filter": "",
			"authenticate": false,
			"template": "<span>{{ item.name }}</span>",
			"multiple": false,
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "select",
			"$$hashKey": "object:15463",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"selectValues": ""
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:8083",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedPanel4",
		"input": false,
		"title": "Radio",
		"theme": "info",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputType": "radio",
			"label": "Radio Inline",
			"key": "undefinedPanel4Radio",
			"values": [{
				"value": "one",
				"label": "one"
			}, {
				"value": "two",
				"label": "two"
			}, {
				"value": "tree",
				"label": "tree"
			}],
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"custom": "",
				"customPrivate": false
			},
			"type": "radio",
			"$$hashKey": "object:9252",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"inline": true
		}, {
			"input": true,
			"tableView": true,
			"inputType": "radio",
			"label": "Radio",
			"key": "undefinedPanel4Radio2",
			"values": [{
				"value": "one",
				"label": "one"
			}, {
				"value": "two",
				"label": "two"
			}, {
				"value": "tree",
				"label": "tree"
			}],
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"custom": "",
				"customPrivate": false
			},
			"type": "radio",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"inline": false,
			"$$hashKey": "object:9959"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:8862",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedPanel5",
		"input": false,
		"title": "Html Element",
		"theme": "success",
		"tableView": false,
		"components": [{
			"key": "undefinedPanel5Html",
			"input": false,
			"tag": "p",
			"attrs": [{
				"value": "",
				"attr": ""
			}],
			"className": "",
			"content": "This is a paragraph",
			"type": "htmlelement",
			"$$hashKey": "object:11255",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}, {
			"key": "undefinedPanel5Html2",
			"input": false,
			"tag": "h1",
			"attrs": [{
				"value": "",
				"attr": ""
			}],
			"className": "",
			"content": "This is a h1",
			"type": "htmlelement",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:11690"
		}, {
			"key": "undefinedPanel5Html3",
			"input": false,
			"tag": "img",
			"attrs": [{
				"value": "https://upload.wikimedia.org/wikipedia/pt/a/ac/Vegeta.jpg",
				"attr": "src"
			}],
			"className": "avatar",
			"content": "This is a h1",
			"type": "htmlelement",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:11955"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:10285",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedPanel6",
		"input": false,
		"title": "Content - Broken. Probably because of CKEDITOR",
		"theme": "default",
		"tableView": false,
		"components": [{
			"key": "undefinedPanel6Content",
			"input": false,
			"html": "",
			"type": "content",
			"$$hashKey": "object:722",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:10714"
	}, {
		"key": "undefinedPanel7",
		"input": false,
		"title": "Button",
		"theme": "primary",
		"tableView": false,
		"components": [{
			"input": true,
			"label": "Event",
			"tableView": false,
			"key": "EventPanel7Submit",
			"size": "md",
			"leftIcon": "glyphicon glyphicon-fire",
			"rightIcon": "glyphicon glyphicon-fire",
			"block": false,
			"action": "event",
			"disableOnInvalid": false,
			"theme": "danger",
			"type": "button",
			"$$hashKey": "object:1701",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"event": "TestEvent",
			"lockKey": true,
			"isNew": false
		}, {
			"input": true,
			"label": "reset",
			"tableView": false,
			"key": "undefinedPanel7Submit",
			"size": "md",
			"leftIcon": "glyphicon glyphicon-refresh",
			"rightIcon": "",
			"block": true,
			"action": "reset",
			"disableOnInvalid": false,
			"theme": "warning",
			"type": "button",
			"$$hashKey": "object:6051",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:672"
	}, {
		"key": "undefinedPanel8",
		"input": false,
		"title": "Email",
		"theme": "info",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputType": "email",
			"label": "Email",
			"key": "undefinedPanel8UndefinedPanel8Email",
			"placeholder": "Email",
			"prefix": "",
			"suffix": "",
			"defaultValue": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"kickbox": {
				"enabled": false
			},
			"type": "email",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Insert your email adress",
			"$$hashKey": "object:4045"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:1664"
	}, {
		"key": "undefinedPanel9",
		"input": false,
		"title": "Phone",
		"theme": "success",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputMask": "(99) 99999-9999",
			"label": "Phone",
			"key": "undefinedPanel9Phone",
			"placeholder": "Your Phone",
			"prefix": "",
			"suffix": "",
			"multiple": false,
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"defaultValue": "",
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "phoneNumber",
			"$$hashKey": "object:4616",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Phone"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:2487"
	}, {
		"key": "undefinedPanel10",
		"input": false,
		"title": "Address",
		"theme": "danger",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Address",
			"key": "undefinedPanel10Adress",
			"placeholder": "type your address",
			"multiple": false,
			"protected": false,
			"clearOnHide": true,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"map": {
				"region": "Brazil",
				"key": ""
			},
			"validate": {
				"required": false
			},
			"type": "address",
			"$$hashKey": "object:5562",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:4276"
	}, {
		"key": "undefinedPanel11",
		"input": false,
		"title": "Date / Time",
		"theme": "warning",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Date only",
			"key": "undefinedPanel11Dateonly",
			"placeholder": "",
			"inputMask": "99/99/9999",
			"format": "dd/MM/yyyy",
			"enableDate": true,
			"enableTime": false,
			"defaultDate": "",
			"datepickerMode": "day",
			"datePicker": {
				"showWeeks": true,
				"startingDay": 0,
				"initDate": "",
				"minMode": "day",
				"maxMode": "year",
				"yearRows": 4,
				"yearColumns": 5,
				"datepickerMode": "day"
			},
			"timePicker": {
				"hourStep": 1,
				"minuteStep": 1,
				"showMeridian": true,
				"readonlyInput": false,
				"mousewheel": true,
				"arrowkeys": true
			},
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"custom": ""
			},
			"type": "datetime",
			"$$hashKey": "object:7162",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Only date format"
		}, {
			"input": true,
			"tableView": true,
			"label": "Time only",
			"key": "undefinedPanel11Dateonly2",
			"placeholder": "",
			"format": "hh:mm",
			"inputMask": "99:99",
			"enableDate": false,
			"enableTime": false,
			"defaultDate": "",
			"datepickerMode": "day",
			"datePicker": {
				"showWeeks": true,
				"startingDay": 0,
				"initDate": "",
				"minMode": "day",
				"maxMode": "year",
				"yearRows": 4,
				"yearColumns": 5,
				"datepickerMode": "day"
			},
			"timePicker": {
				"hourStep": 1,
				"minuteStep": 1,
				"showMeridian": true,
				"readonlyInput": false,
				"mousewheel": true,
				"arrowkeys": true
			},
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"custom": ""
			},
			"type": "datetime",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Only time format",
			"$$hashKey": "object:11958"
		}, {
			"input": true,
			"tableView": true,
			"label": "",
			"key": "undefinedPanel11DatetimeField",
			"placeholder": "",
			"format": "dd/MM/yyyy hh:mm a",
			"enableDate": true,
			"enableTime": true,
			"defaultDate": "",
			"datepickerMode": "day",
			"datePicker": {
				"showWeeks": true,
				"startingDay": 0,
				"initDate": "",
				"minMode": "day",
				"maxMode": "year",
				"yearRows": 4,
				"yearColumns": 5,
				"datepickerMode": "day"
			},
			"timePicker": {
				"hourStep": 1,
				"minuteStep": 1,
				"showMeridian": true,
				"readonlyInput": false,
				"mousewheel": true,
				"arrowkeys": true
			},
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"custom": ""
			},
			"type": "datetime",
			"$$hashKey": "object:13546",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Date and time"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:5209"
	}, {
		"key": "undefinedPanel12",
		"input": false,
		"title": "Day",
		"theme": "default",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Day",
			"key": "undefinedPanel12Day",
			"fields": {
				"day": {
					"type": "number",
					"placeholder": "day",
					"required": false
				},
				"month": {
					"type": "select",
					"placeholder": "month",
					"required": false
				},
				"year": {
					"type": "number",
					"placeholder": "year",
					"required": false
				}
			},
			"dayFirst": true,
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"custom": ""
			},
			"type": "day",
			"$$hashKey": "object:15672",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:6793"
	}, {
		"key": "undefinedPanel13",
		"input": false,
		"title": "Time",
		"theme": "primary",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputType": "time",
			"format": "HH:mm",
			"label": "Time",
			"key": "undefinedPanel13TimeField",
			"placeholder": "select the time",
			"prefix": "",
			"suffix": "",
			"defaultValue": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"type": "time",
			"$$hashKey": "object:17625",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:14531"
	}, {
		"key": "undefinedPanel14",
		"input": false,
		"title": "Currency",
		"theme": "info",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"inputType": "text",
			"inputMask": "",
			"label": "Currency",
			"key": "undefinedPanel14UndefinedPanel14Currency",
			"placeholder": "",
			"prefix": "",
			"suffix": "",
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"multiple": "",
				"custom": ""
			},
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"type": "currency",
			"tags": [],
			"properties": {
				"": ""
			},
			"$$hashKey": "object:20428"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:16826"
	}, {
		"key": "undefinedPanel15",
		"input": false,
		"title": "Hidden",
		"theme": "success",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"key": "undefinedPanel15Hidden",
			"label": "Hidden",
			"protected": false,
			"unique": false,
			"persistent": true,
			"type": "hidden",
			"$$hashKey": "object:21251",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:19170"
	}, {
		"key": "undefinedUndefinedPanel16",
		"input": false,
		"title": "Resource",
		"theme": "danger",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "",
			"key": "undefinedPanel16UndefinedResourceField2",
			"placeholder": "",
			"resource": "",
			"project": "",
			"defaultValue": "",
			"template": "<span>{{ item.data }}</span>",
			"selectFields": "",
			"searchFields": "",
			"multiple": false,
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"defaultPermission": "",
			"type": "resource",
			"$$hashKey": "object:23662"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:23651"
	}, {
		"key": "undefinedPanel17",
		"input": false,
		"title": "File",
		"theme": "warning",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "File",
			"key": "undefinedPanel17File",
			"image": false,
			"imageSize": "200",
			"placeholder": "",
			"multiple": false,
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"type": "file",
			"$$hashKey": "object:24083",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"storage": "url",
			"url": "/upload",
			"dir": "/upload"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:22397"
	}, {
		"key": "undefinedPanel19",
		"input": false,
		"title": "Form - Broken. Probably missing formio specific config",
		"theme": "default",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"key": "undefinedPanel19FormField",
			"src": "",
			"reference": true,
			"form": "",
			"path": "",
			"label": "",
			"protected": false,
			"unique": false,
			"persistent": true,
			"type": "form",
			"$$hashKey": "object:27892"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:25778"
	}, {
		"key": "undefinedPanel20",
		"input": false,
		"title": "Signature",
		"theme": "primary",
		"tableView": false,
		"components": [],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:27407"
	}, {
		"key": "undefinedUndefinedUndefinedPanel21",
		"input": false,
		"title": "Custom - Broken. Don't have a clue why",
		"theme": "info",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Signature",
			"key": "undefinedUndefinedUndefinedPanel21UndefinedUndefinedUndefinedPanel21UndefinedPanel20Signature",
			"placeholder": "",
			"footer": "Sign above",
			"width": "100%",
			"height": "150px",
			"penColor": "black",
			"backgroundColor": "rgb(245,245,235)",
			"minWidth": "0.5",
			"maxWidth": "2.5",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "signature",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:2070",
			"hideLabel": true
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:30968"
	}, {
		"key": "undefinedPanel22",
		"input": false,
		"title": "Container",
		"theme": "success",
		"tableView": false,
		"components": [{
			"input": true,
			"tree": true,
			"components": [{
				"input": true,
				"tableView": true,
				"inputType": "text",
				"inputMask": "",
				"label": "Text Field inside container",
				"key": "undefinedPanel22ContainerTextFieldinsidecontainer",
				"placeholder": "",
				"prefix": "",
				"suffix": "",
				"multiple": false,
				"defaultValue": "",
				"protected": false,
				"unique": false,
				"persistent": true,
				"hidden": false,
				"clearOnHide": true,
				"validate": {
					"required": false,
					"minLength": "",
					"maxLength": "",
					"pattern": "",
					"custom": "",
					"customPrivate": false
				},
				"conditional": {
					"show": "",
					"when": null,
					"eq": ""
				},
				"type": "textfield",
				"$$hashKey": "object:34124",
				"tags": [],
				"properties": {
					"": ""
				}
			}],
			"tableView": true,
			"label": "Container",
			"key": "container",
			"protected": false,
			"persistent": true,
			"clearOnHide": true,
			"type": "container",
			"$$hashKey": "object:33247",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"lockKey": true
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:30013"
	}, {
		"key": "undefinedPanel23",
		"input": false,
		"title": "Data Grid",
		"theme": "danger",
		"tableView": false,
		"components": [{
			"input": true,
			"tree": true,
			"components": [{
				"input": true,
				"tableView": true,
				"inputType": "text",
				"inputMask": "",
				"label": "Text Field inside Data Grid",
				"key": "undefinedPanel23DataGridTextFieldinsideDataGrid",
				"placeholder": "",
				"prefix": "",
				"suffix": "",
				"multiple": false,
				"defaultValue": "",
				"protected": false,
				"unique": false,
				"persistent": true,
				"hidden": false,
				"clearOnHide": true,
				"validate": {
					"required": false,
					"minLength": "",
					"maxLength": "",
					"pattern": "",
					"custom": "",
					"customPrivate": false
				},
				"conditional": {
					"show": "",
					"when": null,
					"eq": ""
				},
				"type": "textfield",
				"$$hashKey": "object:37435",
				"hideLabel": true,
				"tags": [],
				"properties": {
					"": ""
				}
			}],
			"tableView": true,
			"label": "Data Grid",
			"key": "undefinedPanel23DataGrid",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"type": "datagrid",
			"$$hashKey": "object:36498",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"addAnother": "Add another"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:32735"
	}, {
		"key": "undefinedPanel24",
		"input": false,
		"title": "Survey",
		"theme": "warning",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Survey",
			"key": "undefinedPanel24Survey",
			"questions": [{
				"value": "one",
				"label": "One",
				"$$hashKey": "object:39051"
			}, {
				"value": "two",
				"label": "Two",
				"$$hashKey": "object:39476"
			}],
			"values": [{
				"value": "foo",
				"label": "Foo"
			}, {
				"value": "bar",
				"label": "Bar"
			}],
			"defaultValue": "",
			"protected": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false,
				"custom": "",
				"customPrivate": false
			},
			"type": "survey",
			"$$hashKey": "object:38939",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:35486"
	}, {
		"key": "undefinedPanel25",
		"input": false,
		"title": "Columns",
		"theme": "default",
		"tableView": false,
		"components": [{
			"input": false,
			"tableView": false,
			"key": "undefinedPanel25Columns",
			"columns": [{
				"components": [{
					"input": true,
					"tableView": true,
					"inputType": "text",
					"inputMask": "",
					"label": "Text Field Inside Columns",
					"key": "undefinedPanel25ColumnsTextFieldInsideColumns",
					"placeholder": "",
					"prefix": "",
					"suffix": "",
					"multiple": false,
					"defaultValue": "",
					"protected": false,
					"unique": false,
					"persistent": true,
					"hidden": false,
					"clearOnHide": true,
					"validate": {
						"required": false,
						"minLength": "",
						"maxLength": "",
						"pattern": "",
						"custom": "",
						"customPrivate": false
					},
					"conditional": {
						"show": "",
						"when": null,
						"eq": ""
					},
					"type": "textfield",
					"tags": [],
					"properties": {
						"": ""
					},
					"$$hashKey": "object:44661"
				}],
				"width": 6,
				"offset": 0,
				"push": 0,
				"pull": 0,
				"$$hashKey": "object:44655"
			}, {
				"components": [{
					"input": true,
					"tableView": true,
					"inputType": "text",
					"inputMask": "",
					"label": "Other text field inside columns",
					"key": "undefinedPanel25ColumnsOthertextFieldinsideColumns",
					"placeholder": "",
					"prefix": "",
					"suffix": "",
					"multiple": false,
					"defaultValue": "",
					"protected": false,
					"unique": false,
					"persistent": true,
					"hidden": false,
					"clearOnHide": true,
					"validate": {
						"required": false,
						"minLength": "",
						"maxLength": "",
						"pattern": "",
						"custom": "",
						"customPrivate": false
					},
					"conditional": {
						"show": "",
						"when": null,
						"eq": ""
					},
					"type": "textfield",
					"tags": [],
					"properties": {
						"": ""
					},
					"$$hashKey": "object:44668"
				}],
				"width": 6,
				"offset": 0,
				"push": 0,
				"pull": 0,
				"$$hashKey": "object:44656"
			}],
			"type": "columns",
			"$$hashKey": "object:41494",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"isNew": false
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:38382"
	}, {
		"key": "undefinedPanel26",
		"input": false,
		"title": "Field Set",
		"theme": "primary",
		"tableView": false,
		"components": [{
			"key": "undefinedPanel26Fieldset",
			"input": false,
			"tableView": false,
			"legend": "Field set",
			"components": [{
				"input": true,
				"tableView": true,
				"inputType": "text",
				"inputMask": "",
				"label": "Text Field Inside Field Set",
				"key": "undefinedPanel26FieldsetTextFieldInsideFieldSet",
				"placeholder": "",
				"prefix": "",
				"suffix": "",
				"multiple": false,
				"defaultValue": "",
				"protected": false,
				"unique": false,
				"persistent": true,
				"hidden": false,
				"clearOnHide": true,
				"validate": {
					"required": false,
					"minLength": "",
					"maxLength": "",
					"pattern": "",
					"custom": "",
					"customPrivate": false
				},
				"conditional": {
					"show": "",
					"when": null,
					"eq": ""
				},
				"type": "textfield",
				"$$hashKey": "object:46900",
				"tags": [],
				"properties": {
					"": ""
				}
			}],
			"type": "fieldset",
			"$$hashKey": "object:45852",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:39920"
	}, {
		"key": "undefinedPanel27",
		"input": false,
		"title": "Table",
		"theme": "info",
		"tableView": false,
		"components": [{
			"input": false,
			"key": "undefinedPanel27Table",
			"numRows": 1,
			"numCols": 3,
			"rows": [
				[{
					"components": [{
						"input": true,
						"tableView": true,
						"inputType": "text",
						"inputMask": "",
						"label": "Tex Field Inside Table",
						"key": "undefinedPanel27TableTexFieldInsideTable",
						"placeholder": "",
						"prefix": "",
						"suffix": "",
						"multiple": false,
						"defaultValue": "",
						"protected": false,
						"unique": false,
						"persistent": true,
						"hidden": false,
						"clearOnHide": true,
						"validate": {
							"required": false,
							"minLength": "",
							"maxLength": "",
							"pattern": "",
							"custom": "",
							"customPrivate": false
						},
						"conditional": {
							"show": "",
							"when": null,
							"eq": ""
						},
						"type": "textfield",
						"$$hashKey": "object:51454",
						"tags": [],
						"properties": {
							"": ""
						}
					}],
					"$$hashKey": "object:49723"
				}, {
					"components": [{
						"input": true,
						"tableView": true,
						"inputType": "text",
						"inputMask": "",
						"label": "Tex Field Inside Table",
						"key": "undefinedPanel27TableTexFieldInsideTable2",
						"placeholder": "",
						"prefix": "",
						"suffix": "",
						"multiple": false,
						"defaultValue": "",
						"protected": false,
						"unique": false,
						"persistent": true,
						"hidden": false,
						"clearOnHide": true,
						"validate": {
							"required": false,
							"minLength": "",
							"maxLength": "",
							"pattern": "",
							"custom": "",
							"customPrivate": false
						},
						"conditional": {
							"show": "",
							"when": null,
							"eq": ""
						},
						"type": "textfield",
						"$$hashKey": "object:52578",
						"tags": [],
						"properties": {
							"": ""
						}
					}],
					"$$hashKey": "object:49724"
				}, {
					"components": [{
						"input": true,
						"tableView": true,
						"inputType": "text",
						"inputMask": "",
						"label": "Tex Field Inside Table",
						"key": "undefinedPanel27TableTexFieldInsideTable3",
						"placeholder": "",
						"prefix": "",
						"suffix": "",
						"multiple": false,
						"defaultValue": "",
						"protected": false,
						"unique": false,
						"persistent": true,
						"hidden": false,
						"clearOnHide": true,
						"validate": {
							"required": false,
							"minLength": "",
							"maxLength": "",
							"pattern": "",
							"custom": "",
							"customPrivate": false
						},
						"conditional": {
							"show": "",
							"when": null,
							"eq": ""
						},
						"type": "textfield",
						"$$hashKey": "object:53716",
						"tags": [],
						"properties": {
							"": ""
						}
					}],
					"$$hashKey": "object:49725"
				}]
			],
			"header": [],
			"caption": "",
			"striped": false,
			"bordered": false,
			"hover": false,
			"condensed": false,
			"tableView": false,
			"type": "table",
			"$$hashKey": "object:49712",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"$$hashKey": "object:45250"
	}, {
		"key": "undefinedPanel16",
		"input": false,
		"title": "Well",
		"theme": "success",
		"tableView": false,
		"components": [{
			"key": "undefinedPanel16Well",
			"input": false,
			"components": [{
				"input": true,
				"tableView": true,
				"inputType": "text",
				"inputMask": "",
				"label": "Text Field Inside Well",
				"key": "undefinedPanel16WellTextFieldInsideWell",
				"placeholder": "",
				"prefix": "",
				"suffix": "",
				"multiple": false,
				"defaultValue": "",
				"protected": false,
				"unique": false,
				"persistent": true,
				"hidden": false,
				"clearOnHide": true,
				"validate": {
					"required": false,
					"minLength": "",
					"maxLength": "",
					"pattern": "",
					"custom": "",
					"customPrivate": false
				},
				"conditional": {
					"show": "",
					"when": null,
					"eq": ""
				},
				"type": "textfield",
				"$$hashKey": "object:4279",
				"tags": [],
				"properties": {
					"": ""
				}
			}],
			"tableView": false,
			"type": "well",
			"$$hashKey": "object:3152",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			}
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:2007",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedPanel18",
		"input": false,
		"title": "Conditional Rendering",
		"theme": "danger",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Choose your favorite Naruto Character",
			"key": "undefinedPanel18ChooseyourfavoriteNarutoCharacter",
			"placeholder": "Choose your character...",
			"data": {
				"values": [{
					"value": "naruto",
					"label": "Naruto",
					"$$hashKey": "object:5221"
				}, {
					"value": "sazuke",
					"label": "Sazuke",
					"$$hashKey": "object:5754"
				}],
				"json": "",
				"url": "",
				"resource": "",
				"custom": ""
			},
			"dataSrc": "values",
			"valueProperty": "",
			"defaultValue": "",
			"refreshOn": "",
			"filter": "",
			"authenticate": false,
			"template": "<span>{{ item.label }}</span>",
			"multiple": false,
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "select",
			"$$hashKey": "object:5092",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Your fate is set"
		}, {
			"key": "undefinedPanel18Html",
			"input": false,
			"tag": "img",
			"attrs": [{
				"value": "https://s-media-cache-ak0.pinimg.com/736x/5d/a0/8d/5da08d24bc4c7d2847ee5dfa1604b114--naruto-shippudden-naruto-pics.jpg",
				"attr": "src"
			}],
			"className": "avatar",
			"content": "",
			"type": "htmlelement",
			"$$hashKey": "object:3300",
			"tags": [],
			"conditional": {
				"show": "true",
				"when": "undefinedPanel18ChooseyourfavoriteNarutoCharacter",
				"eq": "naruto"
			},
			"properties": {
				"": ""
			}
		}, {
			"key": "undefinedPanel18Html2",
			"input": false,
			"tag": "img",
			"attrs": [{
				"value": "http://pre00.deviantart.net/0a8d/th/pre/f/2016/029/e/a/sasuke_rinnegan_by_simon0405_2016_by_simon0405-d9pql2e.jpg",
				"attr": "src"
			}],
			"className": "avatar",
			"content": "",
			"type": "htmlelement",
			"tags": [],
			"conditional": {
				"show": "true",
				"when": "undefinedPanel18ChooseyourfavoriteNarutoCharacter",
				"eq": "sazuke"
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:6284"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"$$hashKey": "object:2104",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		}
	}, {
		"key": "undefinedConditionalRenderingPanel21",
		"input": false,
		"title": "Advanced Conditional Rendering",
		"theme": "danger",
		"tableView": false,
		"components": [{
			"input": true,
			"tableView": true,
			"label": "Choose your favorite Death Note Character",
			"key": "DeathNoteSelector",
			"placeholder": "Choose your character...",
			"data": {
				"values": [{
					"value": "light",
					"label": "Yagami Light",
					"$$hashKey": "object:4502"
				}, {
					"value": "l",
					"label": "L",
					"$$hashKey": "object:4503"
				}],
				"json": "",
				"url": "",
				"resource": "",
				"custom": ""
			},
			"dataSrc": "values",
			"valueProperty": "",
			"defaultValue": "",
			"refreshOn": "",
			"filter": "",
			"authenticate": false,
			"template": "<span>{{ item.label }}</span>",
			"multiple": false,
			"protected": false,
			"unique": false,
			"persistent": true,
			"hidden": false,
			"clearOnHide": true,
			"validate": {
				"required": false
			},
			"type": "select",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"description": "Your fate is set",
			"$$hashKey": "object:3027",
			"lockKey": true
		}, {
			"key": "undefinedPanel18Html3",
			"input": false,
			"tag": "img",
			"attrs": [{
				"value": "http://pa1.narvii.com/6400/3b35a5ec48b671d27b9f5e29acf6765ed7d18255_hq.gif",
				"attr": "src"
			}],
			"className": "avatar",
			"content": "",
			"type": "htmlelement",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:3028",
			"customConditional": "show = data['DeathNoteSelector'] === 'light'"
		}, {
			"key": "undefinedPanel18Html4",
			"input": false,
			"tag": "img",
			"attrs": [{
				"value": "https://s-media-cache-ak0.pinimg.com/originals/4f/85/13/4f8513c6199a3b56414e8f25f4785617.png",
				"attr": "src"
			}],
			"className": "avatar",
			"content": "",
			"type": "htmlelement",
			"tags": [],
			"conditional": {
				"show": "",
				"when": null,
				"eq": ""
			},
			"properties": {
				"": ""
			},
			"$$hashKey": "object:3029",
			"customConditional": "show = data['DeathNoteSelector'] === 'l'"
		}],
		"type": "panel",
		"breadcrumb": "default",
		"tags": [],
		"conditional": {
			"show": "",
			"when": null,
			"eq": ""
		},
		"properties": {
			"": ""
		},
		"lockKey": true,
		"$$hashKey": "object:3016"
	}, {
		"type": "button",
		"theme": "primary",
		"disableOnInvalid": true,
		"action": "submit",
		"block": false,
		"rightIcon": "",
		"leftIcon": "",
		"size": "md",
		"key": "submit",
		"tableView": false,
		"label": "Submit",
		"input": true,
		"$$hashKey": "object:21"
	}],
	"display": "form",
	"page": 0
}
`);
